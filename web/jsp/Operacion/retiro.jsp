<%-- 
    Document   : retiro
    Created on : 25/10/2019, 09:51:39 AM
    Author     : EN
--%>

<%@page import="Negocio.Banco"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%
        Banco banco = (Banco) request.getSession().getAttribute("banco");
        request.getSession().setAttribute("banco", banco);

    %>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Retiro</h1>
        <form name="retiro" action="../../Retirar.do" method="POST">
            <p>Nro de cuenta de la que se retira el dinero<input name="nroCuenta" type="text" maxlength="10"/></p> <br>
            <p>Saldo a retirar<input name="saldo" type="number" maxlength="9"/></p> <br>
            <input type="submit" value="Retirar" name="Retirar" />
        </form>
    </body>
</html>
