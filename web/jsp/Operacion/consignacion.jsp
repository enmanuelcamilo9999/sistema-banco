<%-- 
    Document   : consignacion
    Created on : 25/10/2019, 09:52:00 AM
    Author     : EN
--%>

<%@page import="Negocio.Banco"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html><%
    Banco banco = (Banco) request.getSession().getAttribute("banco");
    request.getSession().setAttribute("banco", banco);
    %>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Consignacion</h1>
        <form name="consignacion" action="../../Consignar.do" method="POST">
            <p>Nro de cuenta que recibe el dinero<input name="nroCuenta" type="text" maxlength="10"/></p> <br>
            <p>Saldo a consignar<input name="saldo" type="number" maxlength="9"/></p> <br>
            <input type="submit" value="Consignar" name="consignar" />
        </form>
    </body>
</html>
