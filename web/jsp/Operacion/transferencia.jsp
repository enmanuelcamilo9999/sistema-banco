<%-- 
    Document   : transferencia
    Created on : 25/10/2019, 09:51:50 AM
    Author     : EN
--%>

<%@page import="Negocio.Banco"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%
        Banco banco = (Banco) request.getSession().getAttribute("banco");
        request.getSession().setAttribute("banco", banco);
    %>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Transferencia</h1>
        <form name="transferencia" action="../../Transferir.do" method="POST">
            <p>Nro de cuenta origen<input name="nroCuentaOrigen" type="text" maxlength="10"/></p> <br>
            <p>Nro de cuenta destino<input name="nroCuentaDestino" type="text" maxlength="10"/></p> <br> 
            <p>Saldo a transferir<input name="saldo" type="number" maxlength="9"/></p> <br>
            <input type="submit" value="Transferir" name="Transferir" />
        </form>
    </body>
</html>
