<%-- 
    Document   : consignacionExito
    Created on : 3/11/2019, 04:16:10 PM
    Author     : EN
--%>

<%@page import="Negocio.Banco"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%
        Banco banco = (Banco) (request.getSession().getAttribute("banco"));
        request.getSession().setAttribute("banco", banco);
        Long ultimaCuenta = Long.parseLong(request.getSession().getAttribute("ultimaCuenta").toString());
        String operaciones= banco.getOperaciones(ultimaCuenta);
        

    %>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Operacion Exitosa</h1>
        <p>Registro de operaciones</p>
        <%=operaciones%>
        
        <a href="./index.jsp">Home</a>
    </body>
</html>
