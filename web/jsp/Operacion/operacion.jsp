<%-- 
    Document   : operacion
    Created on : 25/10/2019, 09:50:48 AM
    Author     : EN
--%>

<%@page import="Negocio.Banco"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%
        Banco banco = (Banco) (request.getSession().getAttribute("banco"));
        request.getSession().setAttribute("banco", banco);
    %>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Elija una operacion</h1>
        <p><a href="./transferencia.jsp">Transferir</a></p>
        <p><a href="./consignacion.jsp">Consignar</a></p>
        <p><a href="./retiro.jsp">Retirar</a></p>
    </body>
</html>
