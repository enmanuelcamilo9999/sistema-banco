<%-- 
    Document   : informeCuentaExitoso
    Created on : 3/11/2019, 06:34:24 PM
    Author     : EN
--%>

<%@page import="Negocio.Banco"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%
        Banco banco = (Banco) (request.getSession().getAttribute("banco"));
        request.getSession().setAttribute("banco", banco);
        String informe = request.getSession().getAttribute("informe").toString();
        
    %>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Informe de la cuenta</h1>
        <br>
        <%=informe%>
        <a href="./index.jsp">Home</a>
    </body>
</html>
