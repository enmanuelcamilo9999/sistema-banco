package Negocio;

import Dto.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

public class Banco {

    TreeSet<Cliente> clientes = new TreeSet();
    TreeSet<Cuenta> cuentas = new TreeSet();

    public Banco() {
    }

    public boolean insertarCliente(long cedula, String nombre, String dir, String fecha, String email, long telefono) {
        Cliente nuevo = new Cliente();
        nuevo.setCedula(cedula);
        nuevo.setNombre(nombre);
        nuevo.setFechaNacimiento(crearFecha(fecha));
        nuevo.setDirCorrespondencia(dir);
        nuevo.setTelefono(telefono);
        nuevo.setEmail(email);
        //iría al dao , pero sólo por hoy en dinamic_memory
        return this.clientes.add(nuevo);
    }

    public boolean insertarCuenta(long nroCuenta, long cedula, boolean tipo) {
        Cliente x = new Cliente();
        x.setCedula(cedula);
        x = buscarCliente(x);
        if (x == null) {
            return false;
        }
        Cuenta nueva = tipo ? new CuentaAhorro() : new CuentaCorriente();
        nueva.setCliente(x);
        nueva.setNroCuenta(nroCuenta);
        nueva.setFechaCreacion(LocalDate.now());
        return this.cuentas.add(nueva);
    }

    private boolean aumentarDinero(long nroCuenta, double saldo, LocalDate fecha) {
        boolean exito = false;
        Cuenta x = new Cuenta();
        x.setNroCuenta(nroCuenta);
        x = buscarCuenta(x);
        if (x != null) {
            x.setSaldo(x.getSaldo() + saldo);
            exito = true;
        }
        return exito;
    }

    public boolean consignar(long nroCuenta, double saldo, LocalDate fecha) {
        boolean exito = false;
        boolean aumentarFueExitoso = aumentarDinero(nroCuenta, saldo, fecha);
        if (aumentarFueExitoso) {
            Cuenta x = new Cuenta();
            x.setNroCuenta(nroCuenta);
            x = buscarCuenta(x);
            Consignacion c = new Consignacion(fecha, saldo, (x.getOperaciones().size() + 1));
            x.getOperaciones().add(c);
            exito = true;
        }
        return exito;
    }

    private boolean disminuirDinero(long nroCuenta, double saldo, LocalDate fecha) {
        boolean exito = false;
        Cuenta x = new Cuenta();
        x.setNroCuenta(nroCuenta);
        x = buscarCuenta(x);
        if (x != null) {
            if (x.getSaldo() < saldo) {
                if (x instanceof CuentaCorriente) {
                    x.setSaldo(((CuentaCorriente) x).getSobregiro() + x.getSaldo());
                }
                if (x.getSaldo() < saldo) {
                    return exito;
                }
            }
            x.setSaldo(x.getSaldo() - saldo);
            exito = true;
        }
        return exito;
    }

    public boolean retirar(long nroCuenta, double saldo, LocalDate fecha) {
        boolean exito = false;
        boolean disminuirFueExitoso = disminuirDinero(nroCuenta, saldo, fecha);
        if (disminuirFueExitoso) {
            Cuenta x = new Cuenta();
            x.setNroCuenta(nroCuenta);
            x = buscarCuenta(x);
            Retiro r = (Retiro) new Operacion(fecha, saldo, (x.getOperaciones().size() + 1));
            x.getOperaciones().add(r);
            exito = true;
        }

        return exito;
    }

    public boolean transferir(long nroCuentaOrigen, long nroCuentaDestino, double saldo, LocalDate fecha) {
        boolean exito = false;
        boolean retiroExito = disminuirDinero(nroCuentaOrigen, saldo, fecha);
        boolean consignacionExito = aumentarDinero(nroCuentaDestino, saldo, fecha);
        if (retiroExito & consignacionExito) {
            Cuenta x = new Cuenta();
            x.setNroCuenta(nroCuentaOrigen);
            x = buscarCuenta(x);
            Cuenta y = new Cuenta();
            y.setNroCuenta(nroCuentaDestino);
            y = buscarCuenta(y);

            Transferencia t = new Transferencia(fecha, saldo, (x.getOperaciones().size() + 1), nroCuentaOrigen, nroCuentaDestino);
            x.getOperaciones().add(t);
            y.getOperaciones().add(t);
            exito = consignacionExito;
        }
        return exito;
    }

    public String getOperaciones(long nroCuenta) {
        String informe = "";
        Cuenta cuenta = new Cuenta();
        cuenta.setNroCuenta(nroCuenta);
        Cuenta buscada = buscarCuenta(cuenta);
        if (buscada != null) {
            informe = buscada.getInformeOperaciones();

        }
        return informe;
    }

    public String getInforme(Long nroCuenta) {
        String informe = "Informe de cuenta: \n";
        Cuenta cuenta = new Cuenta();
        cuenta.setNroCuenta(nroCuenta);
        Cuenta buscada = buscarCuenta(cuenta);
        if (buscada != null) {
            informe += "Saldo: " + buscada.getSaldo() + "\nCliente:" + buscada.getCliente()+ buscada.getInformeOperaciones();

        }
        return informe;
    }

    public String getInforme(int cedula) {
        String informe = "Informe de cuentas del cliente"+cedula+": \n";
        Cliente cliente = new Cliente();
        cliente.setCedula(cedula);
        List<Cuenta> cuentasCliente = buscarCuentas(cliente);

        int tamanio = cuentasCliente.size();
        for (int i = 0; i < tamanio; i++) {
            informe += "*Saldo: " + cuentasCliente.get(i).getSaldo() + "\nCliente:"
                    + cuentasCliente.get(i).getCliente() + cuentasCliente.get(i).getInformeOperaciones();
        }

        return informe;
    }

    private Cuenta buscarCuenta(Cuenta x) {
        if (this.cuentas.contains(x)) {
            return this.cuentas.floor(x);
        }
        return null;
    }

    private List<Cuenta> buscarCuentas(Cliente x) {
        List<Cuenta> cuentas = new ArrayList<>();
        if (this.clientes.contains(x)) {
            for (Cuenta cuentaAct : this.cuentas) {
                if (cuentaAct.getCliente().getCedula() == x.getCedula()) {
                    cuentas.add(cuentaAct);
                }
            }
        }

        return cuentas;
    }

    private Cliente buscarCliente(Cliente x) {
        if (this.clientes.contains(x)) {
            return this.clientes.floor(x);
        }
        return null;
    }

    private Cliente buscarCliente2(Cliente x) {
        for (Cliente y : this.clientes) {
            if (y.equals(x)) {
                return y;
            }
        }
        return null;
    }

    private LocalDate crearFecha(String fecha) {
        String fechas[] = fecha.split("-");
        int agno = Integer.parseInt(fechas[0]);
        int mes = Integer.parseInt(fechas[1]);
        int dia = Integer.parseInt(fechas[2]);
        return LocalDate.of(agno, mes, dia);
    }

    public TreeSet<Cliente> getClientes() {
        return clientes;
    }

    public TreeSet<Cuenta> getCuentas() {
        return cuentas;
    }

}
