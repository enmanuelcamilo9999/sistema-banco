/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Negocio.Banco;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author EN
 */
public class Transferir extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       try {
            Banco banco= (Banco)request.getSession().getAttribute("banco");
            request.getSession().setAttribute("banco", banco);
            
            if (banco==null) {
                request.getSession().setAttribute("error", "El banco no fue encontrado");
                request.getRequestDispatcher("./jsp/error/errorbanco.jsp").forward(request, response);
                
            }
            else{
                
               long nroCuentaOrigen=Long.parseLong(request.getParameter("nroCuentaOrigen"));
               long nroCuentaDestino=Long.parseLong(request.getParameter("nroCuentaDestino"));
               double saldo= Double.parseDouble(request.getParameter("saldo"));
               LocalDate fecha=null;
               
               boolean fueExitoso=banco.transferir(nroCuentaOrigen, nroCuentaDestino, saldo, fecha);
                if (fueExitoso) {
                    request.getSession().setAttribute("ultimaCuenta", nroCuentaOrigen);
                    request.getRequestDispatcher("./jsp/Operacion/operacionExito.jsp").forward(request, response);
                }
               
            
            }
            
            
            
        } catch (Exception e) {
            System.err.println(e.getMessage());
            request.getSession().setAttribute("error", e.getMessage());
            request.getRequestDispatcher("./jsp/error/error.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
