
package Dto;

import java.time.LocalDate;
import java.util.TreeSet;

public class Cuenta implements Comparable{
    
    private long nroCuenta;
    private Cliente cliente;
    private double saldo;
    private LocalDate fechaCreacion;
    TreeSet<Operacion> operaciones = new TreeSet();

    public TreeSet<Operacion> getOperaciones() {
        return operaciones;
    }

    public void setOperaciones(TreeSet<Operacion> operaciones) {
        this.operaciones = operaciones;
    }
    
    public Cuenta() {
    }

    public long getNroCuenta() {
            return nroCuenta;
    }

    public void setNroCuenta(long nroCuenta) {
        this.nroCuenta = nroCuenta;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public LocalDate getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(LocalDate fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }
    
    public String getInformeOperaciones(){
    
        String informe="";
        for (Operacion operacion:operaciones){
              informe+= operacion.toString()+"\n";
        }
        return informe;
    }

    @Override
    public int compareTo(Object o) {
      Cuenta cuenta=(Cuenta)o;
      return (int)(this.nroCuenta-cuenta.nroCuenta);
    }

    @Override
    public String toString() {
        return "Cuenta{" + "nroCuenta=" + nroCuenta + ", cliente=" + cliente.getCedula() + ", saldo=" + saldo + ", fechaCreacion=" + fechaCreacion + '}';
    }
    
    
    
    
}
