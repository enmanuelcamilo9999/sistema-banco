/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dto;

import java.time.LocalDate;

/**
 *
 * @author EN
 */
public class Transferencia extends Operacion {

    private long nroCuentaOrigen;
    private long nroCuentaDestino;
    
    public Transferencia(LocalDate fecha, double saldo, int identificador,long nroCuentaOrigen, long nroCuentaDestino) {
        super(fecha, saldo, identificador);
        this.nroCuentaOrigen = nroCuentaOrigen;
        this.nroCuentaDestino = nroCuentaDestino;
    }

    public long getNroCuentaOrigen() {
        return nroCuentaOrigen;
    }

    public void setNroCuentaOrigen(long nroCuentaOrigen) {
        this.nroCuentaOrigen = nroCuentaOrigen;
    }

    public long getNroCuentaDestino() {
        return nroCuentaDestino;
    }

    public void setNroCuentaDestino(long nroCuentaDestino) {
        this.nroCuentaDestino = nroCuentaDestino;
    }

    @Override
    public String toString() {
        return "Tranferencia de $"+this.saldo+" desde la cuenta "+this.nroCuentaOrigen+" hacia la cuenta "+this.nroCuentaDestino;
    }

    
}
