/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dto;

import java.time.LocalDate;

/**
 *
 * @author EN
 */
public class Consignacion extends Operacion {

    public Consignacion() {
        super();
    }
    
    public Consignacion(LocalDate fecha, Double saldo,int identificador){
      super(fecha, saldo, identificador);
    }

    @Override
    public String toString() {
        return "Consignacion de $"+ this.saldo+" hecha el día ####/##/##";
    }

    
    
    
    
}
