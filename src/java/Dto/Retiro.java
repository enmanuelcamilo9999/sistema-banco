/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dto;

import java.time.LocalDate;

/**
 *
 * @author EN
 */
public class Retiro extends Operacion{

    public Retiro() {
        super();
    }
    public Retiro(LocalDate fecha, Double saldo,int identificador){
      super(fecha, saldo, identificador);
    }
   
    @Override
    public String toString() {
        return "Retiro de $"+ this.saldo+" hecho el día ####/##/##";
    }
}
